#!/usr/bin/env bash

# https://github.com/ralish/bash-script-template/blob/stable/template.sh
set -o errexit  # Exit on most errors (see the manual)
set -o pipefail # Use last non-zero exit code in a pipeline

# DESC: Initializes the script.
function script_init() {
  readonly INITIALIZED_FILE_PATH='/.initialized'
  readonly APACHE_CONFIGURATION_PATH='/docker-entrypoint-init.d/httpd'
  readonly APACHE_VHOST_CONFIGURATION_PATH='/docker-entrypoint-init.d/httpd/conf.d'
  readonly APACHE_MODULES_CONFIGURATION_PATH='/docker-entrypoint-init.d/httpd/conf.modules.d'
  readonly SCRIPTS_PATH='/docker-entrypoint-init.d/scripts'
}

# DESC: Creates both user and group using provided ennvironment variables.
create_user_and_group() {
  echo "Create both user and group..."

  if [[ $(id -u app > /dev/null 2>&1; echo $?) == "0" ]]; then
    echo "User app already exists."
    return 0
  fi

  if [[ -z "${HOST_USER_ID}" || -z "${HOST_GROUP_ID}" ]]; then
    echo "You have to define both HOST_USER_ID and HOST_GROUP_ID environment variables."
    exit 1
  fi

  groupadd -g "${HOST_GROUP_ID}" app && useradd -u "${HOST_USER_ID}" -g app -s /bin/bash app
}

# DESC: Copies Apache configuration.
copy_apache_configuration() {
  if [[ -d "${APACHE_CONFIGURATION_PATH}" ]]; then
    if [[ -e "${APACHE_CONFIGURATION_PATH}/httpd.conf" ]]; then
      echo "Copy ${APACHE_CONFIGURATION_PATH}/httpd.conf to /etc/httpd/conf/httpd.conf"
      \cp ${APACHE_CONFIGURATION_PATH}/httpd.conf /etc/httpd/conf/httpd.conf
    fi
    # Apache vhost configuration folder
    if [[ -d "${APACHE_VHOST_CONFIGURATION_PATH}" ]]; then
      for apache_vhost_configuration in "${APACHE_VHOST_CONFIGURATION_PATH}"/*.conf; do
        [ -e "$apache_vhost_configuration" ] || continue
        echo "Copy ${apache_vhost_configuration} in /etc/httpd/conf.d folder."
        \cp "$apache_vhost_configuration" /etc/httpd/conf.d/
      done
    else
      echo "You have not defined a directory containing additional Apache vhost configuration at path ${APACHE_VHOST_CONFIGURATION_PATH}."
    fi
    # Apache modules configuration folder
    if [[ -d "${APACHE_MODULES_CONFIGURATION_PATH}" ]]; then
      for apache_modules_configuration in "${APACHE_MODULES_CONFIGURATION_PATH}"/*.conf; do
        [ -e "$apache_modules_configuration" ] || continue
        echo "Copy ${apache_modules_configuration} in /etc/httpd/conf.modules.d folder."
        \cp "$apache_modules_configuration" /etc/httpd/conf.modules.d/
      done
    else
      echo "You have not defined a directory containing additional Apache modules configuration at path ${APACHE_MODULES_CONFIGURATION_PATH}."
    fi
  else
    echo "You have not defined a directory containing additional Apache configuration at path ${APACHE_CONFIGURATION_PATH}."
  fi
}

# DESC: Executes additional scripts.
execute_scripts() {
  if [[ -d "${SCRIPTS_PATH}" ]]; then
    for script in "${SCRIPTS_PATH}"/*.sh; do
      if [[ -x "${script}" ]]; then
        echo "Running ${script}."
        . "${script}"
      else
        echo "Script ${script} is not executable. It will be skipped."
      fi
    done
  else
    echo "You have not defined a directory containing additional scripts at path ${SCRIPTS_PATH}."
  fi
}

# DESC: Initializes the container.
initialize() {
  create_user_and_group
  copy_apache_configuration
  execute_scripts

  touch ${INITIALIZED_FILE_PATH}
}

# DESC: Starts httpd.
start_httpd() {
  echo "Start httpd..."

  # Apache won't start if the PID file already exists for any reason.
  rm -rf /run/httpd/httpd.pid

  /usr/sbin/httpd -D FOREGROUND
}


# DESC: Checks whether the container is initialized or not. If not, initializes it. After that run httpd.
main() {
  script_init

  if [[ ! -e "${INITIALIZED_FILE_PATH}" ]]; then
    echo "Initialize Apache container..."
    initialize
  else
    echo "Apache container is already initialized."
  fi

  start_httpd
}

main "$@"